# AVM Fritzbox 7360 v2 Debrick Flash



## Main Issue

Trying to flash my AVM Fritzbox 7360 v2 (https://boxmatrix.info/wiki/FRITZ!Box_Fon_WLAN_7360_v2) I got it bricked and even the lights seem correct I could not connect via ssh/ftp in any of the possible IPs I configured before in the router.

This page should help to document how I managed to reflash it back to openwrt but it likely should work for other firmwares since I followed some instructions for gluon OS.

I did the flashing from a Manjaro machine but this instructions should work from any Linux Terminal.


## Files Needed

- Flash Tool ==> fritzflash.py --> https://gitlab.com/cpr_polo/fritzbox-7360-v2-debrick-flash/-/blob/main/fritzflash.py?ref_type=heads
- Firmware (OpenWrt) ==> https://downloads.openwrt.org/releases/22.03.5/targets/lantiq/xrx200/openwrt-22.03.5-lantiq-xrx200-avm_fritz7360-v2-squashfs-sysupgrade.bin ==> https://gitlab.com/cpr_polo/fritzbox-7360-v2-debrick-flash/-/blob/main/openwrt-22.03.5-lantiq-xrx200-avm_fritz7360-v2-squashfs-sysupgrade.bin?ref_type=heads

## Software Needed
- Python 3

## Instructions

Partial Source: https://fritz-tools.readthedocs.io/en/latest/flashing/ubuntu_1804.html

1. From a Terminal, download the flashing tool for fritzbox:  
```
wget https://raw.githubusercontent.com/freifunk-darmstadt/fritz-tools/master/fritzflash.py .
```

2. Download the firmware needed (in my case, OpenWrt): 
https://openwrt.org/toh/avm/avm_fritz_box_7360_v2

3. Connect your computer to the LAN1 of the AVM Fritzbox 7360 v2 via an Ethernet Cable
4. Set the connected interface to use Static IP as the below:
    IP Address: 192.168.178.2
    Subnet Mask: 255.255.255.0
    Gateway: 192.168.178.1
5. Open the terminal and run the flashing tool with the required image:
```
python3 fritzflash.py --image openwrt-22.03.5-lantiq-xrx200-avm_fritz7360-v2-squashfs-sysupgrade.bin
```
It will start flashing
6. When finished, revert the Interface changes and leave it as DHCP so that the Router can serve an IP
7. If all went well, your PC should have an IP Address in the range 192.168.1.1/24 (default OpenWrt range, other firmware may vary)
8. Open a browser and browse to 192.168.1.1 for the management interface of OpenWrt in your AVM Fritzbox 7360 v2

# Considerations
- The IP Address range needs to be as explained above, other interfaces in the PC (wifi, ...) may conflict, so recommended to have one interface active at the time.

- There may be newer firmware files for OpenWrt when you see this, so check the openwrt website.
- As far as I know, this should work for other Fritzbox Routers, but I have only tested it in AVM Fritzbox 7360 v2

# Responsability
- **I am taking no responsability** on what the status of your router is before or after following these instructions
- Feel free to ask me questions if you have them and I will try my best to help you
